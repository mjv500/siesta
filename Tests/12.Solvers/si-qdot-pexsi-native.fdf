SystemName          Si Quantum Dot with native PEXSI interface
SystemLabel         si-qdot-pexsi-native

use-tree-timer T
DirectPhi T

SolutionMethod        pexsi   # Need to compile SIESTA with -DSIESTA__PEXSI (see Manual)
#
# With these numbers, full parallelization in PEXSI will be achieved with 40 procs
PEXSI.np-per-pole 2                # Number of procs per pole-team
PEXSI.num-poles 20
#-------------------------
# It might be necessary to reduce the number of nodes working on the non-solver parts
#
#MPI.Nprocs.SIESTA 120             # Set this for non-solver steps
#
#-------------------------------------------- The rest should not need to be changed,
#                                             except this, if we know an approx value:
PEXSI.mu  -0.30 Ry
#
PEXSI.ordering 1
PEXSI.np-symbfact 16

PEXSI.inertia-count 1
PEXSI.inertia-counts 100
PEXSI.inertia-min-num-shifts 40
PEXSI.inertia-num-electron-tolerance 150
PEXSI.mu-max-iter 30
PEXSI.temperature 300.0
PEXSI.muMin -2.00 Ry
PEXSI.muMax 0.00 Ry
PEXSI.num-electron-tolerance-upper-bound 1
PEXSI.num-electron-tolerance-lower-bound 0.01
DM.NormalizationTolerance 1.0d-1  # (true_no_electrons/no_electrons) - 1.0 
#

#--------
# For a quicker test. Remove if full test is desired
MaxSCFIterations     1
scf-must-converge F
compute-forces F
#--------

WriteDMHS.NetCDF F
WriteDM.NetCDF F
WriteDM T

%block ChemicalSpeciesLabel
 1  14  Si
 2   1  H 
%endblock ChemicalSpeciesLabel

# CSIRO quantum dot
%include coords.Si987H372.fdf

PAO.BasisSize       SZ
PAO.EnergyShift     300 meV

#
LatticeConstant     45.0 Ang
%block LatticeVectors
  1.000  0.000  0.000
  0.000  1.000  0.000
  0.000  0.000  1.000
%endblock LatticeVectors

MeshCutoff          150.0 Ry

MaxSCFIterations    40
DM.MixingWeight      0.3 
DM.NumberPulay       4  
DM.Tolerance         1.d-3
DM.UseSaveDM             

ElectronicTemperature  25 meV  
