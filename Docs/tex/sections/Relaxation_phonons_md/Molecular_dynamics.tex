In this mode of operation, the program moves the atoms (and optionally
the cell vectors) in response to the forces (and stresses), using the
classical equations of motion.

Note that the \fdf{Zmatrix} input option (see Sec.~\ref{sec:Zmatrix}) is not
compatible with molecular dynamics. The initial geometry can be
specified using the Zmatrix format, but the Zmatrix generalized
coordinates will not be updated.


\begin{fdfentry}{MD.InitialTimeStep}[integer]<$1$>

  Initial time step of the MD simulation.  In the current version of
  \siesta\ it must be 1.

  Used only if \fdf{MD.TypeOfRun} is not \fdf*{CG} or \fdf*{Broyden}.

\end{fdfentry}

\begin{fdfentry}{MD.FinalTimeStep}[integer]<\fdfvalue{MD.Steps}>

  Final time step of the MD simulation.

\end{fdfentry}


\begin{fdfentry}{MD.LengthTimeStep}[time]<$1\,\mathrm{fs}$>

  Length of the time step of the MD simulation.

\end{fdfentry}

\begin{fdfentry}{MD.InitialTemperature}[temperature/energy]<$0\,\mathrm K$>

  Initial temperature for the MD run. The atoms are assigned random
  velocities drawn from the Maxwell-Bolzmann distribution with the
  corresponding temperature. The constraint of zero center of mass
  velocity is imposed.

  \note only used if \fdf{MD.TypeOfRun} \fdf*{Verlet}, \fdf*{Nose},
  \fdf*{ParrinelloRahman}, \fdf*{NoseParrinelloRahman} or
  \fdf*{Anneal}.

\end{fdfentry}

\begin{fdfentry}{MD.TargetTemperature}[temperature/energy]<$0\,\mathrm K$>

  Target temperature for Nose thermostat and annealing options.

  \note only used if \fdf{MD.TypeOfRun} \fdf*{Nose},
  \fdf*{NoseParrinelloRahman} or
  \fdf*{Anneal} if \fdf{MD.AnnealOption} is \fdf*{Temperature} or
  \fdf*{TemperatureandPressure}.

\end{fdfentry}

\begin{fdfentry}{MD.NoseMass}[moment of inertia]<$100\,\mathrm{Ry\,fs^2}$>

  Generalized mass of Nose variable.  This determines the time scale
  of the Nose variable dynamics, and the coupling of the thermal bath
  to the physical system.

  Only used for Nose MD runs.

\end{fdfentry}

\begin{fdfentry}{MD.ParrinelloRahmanMass}[moment of inertia]<$100\,\mathrm{Ry\,fs^2}$>

  Generalized mass of Parrinello-Rahman variable.  This determines the
  time scale of the Parrinello-Rahman variable dynamics, and its
  coupling to the physical system.

  Only used for Parrinello-Rahman MD runs.

\end{fdfentry}

\begin{fdfentry}{MD.AnnealOption}[string]<TemperatureAndPressure>

  Type of annealing MD to perform. The target temperature or pressure
  are achieved by velocity and unit cell rescaling, in a given time
  determined by the variable \fdf{MD.TauRelax} below. These are based on
  the Berendsen thermostat and barostats, respectively \cite{Berendsen84}.
  \begin{fdfoptions}
    \option[Temperature]%
    Reach a target temperature by velocity rescaling

    \option[Pressure]%
    Reach a target pressure by scaling of the unit cell size and shape

    \option[TemperatureandPressure]%
    Reach a target temperature and pressure by velocity rescaling and
    by scaling of the unit cell size and shape
  \end{fdfoptions}

  Only applicable for \fdf{MD.TypeOfRun:Anneal}.

\end{fdfentry}

\begin{fdfentry}{MD.TauRelax}[time]<$100\,\mathrm{fs}$>

  Relaxation time to reach target temperature and/or pressure in
  annealing MD. Note that this is a ``relaxation time'', and as such
  it gives a rough estimate of the time needed to achieve the given
  targets. As a normal simulation also exhibits oscillations, the
  actual time needed to reach the \emph{averaged} targets will be
  significantly longer.

  When using the barostat, the actual time required to reach the target
  pressure will depend on the ratio between \fdf{MD.TauRelax} and
  \fdf{MD.BulkModulus}.

  Only applicable for \fdf{MD.TypeOfRun:Anneal}.

\end{fdfentry}

\begin{fdfentry}{MD.BulkModulus}[pressure]<$100\,\mathrm{Ry/Bohr^3}$>

  Estimate (may be rough) of the bulk modulus of the system.  This is
  needed to set the rate of change of cell shape to reach target
  pressure in annealing MD.

  Only applicable for \fdf{MD.TypeOfRun} \fdf*{Anneal}, when
  \fdf{MD.AnnealOption} is \fdf*{Pressure} or \fdf*{TemperatureAndPressure}

\end{fdfentry}
