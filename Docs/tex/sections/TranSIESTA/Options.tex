The fdf options shown here are only to be used at the input file for
the scattering region. When using \tsiesta\ for electrode
calculations, only the usual \siesta\ options are relevant.
%
Note that since \tsiesta\ is a generic $\Nelec$ electrode NEGF code the
input options are heavily changed compared to versions prior to 4.1.

\subsubsection{Quick and dirty}

Since 4.1, \tsiesta\ has been fully re-implemented. And so have
\emph{every} input fdf-flag. To accommodate an easy transition between
previous input files and the new version format a small utility called
\program{ts2ts}. It may be compiled in \program{Util/TS/ts2ts}. It is
recommended that you use this tool if you are familiar with previous
\tsiesta\ versions.

%
One may input options as in the old \tsiesta\ version and then run
\begin{fdfexample}
  ts2ts OLD.fdf > NEW.fdf
\end{fdfexample}
which translates all keys to the new, equivalent, input format. If you
are familiar with the old-style flags this is highly recommendable
while becoming comfortable with the new input format. Please note that
some defaults have changed to more conservative values in the newer
release.

If one does not know the old flags and wish to get a basic example of
an input file, a script \program{Util/TS/tselecs.sh} exists that can
create the basic input for $\Nelec$ electrodes. One may call it like:
\begin{shellexample}
  tselecs.sh -2 > TWO_ELECTRODE.fdf
  tselecs.sh -3 > THREE_ELECTRODE.fdf
  tselecs.sh -4 > FOUR_ELECTRODE.fdf
  ...
\end{shellexample}
where the first call creates an input fdf for 2 electrode setups, the
second for a 3 electrode setup, and so on. See the help (\program{-h})
for the program for additional options.

Before endeavoring on large scale calculations you are advised to run
an analyzation of the system at hand, you may run your system as
\begin{shellexample}
  siesta -fdf TS.Analyze RUN.fdf > analyze.out
\end{shellexample}
which will analyze the sparsity pattern and print out several
different pivoting schemes. Please see \fdf{TS!Analyze} for more
information.


\subsubsection{General options}

One have to set \fdf{SolutionMethod} to \fdf*{transiesta} to enable
\tsiesta.

\begin{fdfentry}{TS!SolutionMethod}[string]<btd|mumps|full>

  Control the algorithm used for calculating the Green
  function. Generally the BTD method is the fastest and this option
  need not be changed.

  \begin{fdfoptions}
    \option[BTD]%
    \fdfindex*{TS!SolutionMethod:BTD}%
    Use the block-tri-diagonal algorithm for matrix inversion.

    This is generally the recommended method.

    \option[MUMPS]%
    \fdfindex*{TS!SolutionMethod:MUMPS}%
    Use sparse matrix inversion algorithm (MUMPS). This requires
    \tsiesta\ to be compiled with MUMPS.
    \index{MUMPS}%
    \index{External library!MUMPS}%

    \option[full]%
    \fdfindex*{TS!SolutionMethod:full}%
    Use full matrix inversion algorithm (LAPACK). Generally only
    usable for debugging purposes.

  \end{fdfoptions}

\end{fdfentry}

\begin{fdfentry}{TS!Voltage}[energy]<$0\,\mathrm{eV}$>

  Define the reference applied bias. For $\Nelec=2$ electrode calculations
  this refers to the actual potential drop between the electrodes,
  while for $\Nelec\neq2$ this is a reference bias. In the latter case it
  \emph{must} be equivalent to the maximum difference between the
  chemical potential of any two electrodes.

  \note Specifying \shell{-V}\fdfindex{Command line options:-V} on the
  command-line overwrites the value in the fdf file.

\end{fdfentry}

\begin{fdfentry}{TS!kgrid!MonkhorstPack}[block]<\fdfvalue{kgrid!MonkhorstPack}>

  $k$ points used for the \tsiesta\ calculation.

  For $\Nelec\neq2$ this should always be defined. Always take care to
  use only 1 $k$ point along non-periodic lattice vectors. An
  electrode semi-infinite region is considered non-periodic since it
  is integrated out through the self-energies.

  This defaults to \fdf{kgrid!MonkhorstPack}.

\end{fdfentry}

\begin{fdfentry}{TS!Atoms.Buffer}[block/list]
  \fdfindex{TS.BufferAtomsLeft|see TS!Atoms.Buffer}%
  \fdfindex{TS.BufferAtomsRight|see TS!Atoms.Buffer}%

  Specify atoms that will be removed in the \tsiesta\ SCF. They are
  not considered in the calculation and may be used to improve the
  initial guess for the Hamiltonian.


  An intended use for buffer atoms is to ensure a bulk behavior in the
  electrode regions when electrodes are different. As an example: a 2
  electrode calculation with left consisting of Au atoms and the right
  consisting of Pt atoms. In such calculations one cannot create a
  periodic geometry along the transport direction. One needs to add
  vacuum between the Au and Pt atoms that comprise the
  electrodes. However, this creates an artificial edge of the
  electrostatic environment for the electrodes since in \siesta\ there
  is vacuum, whereas in \tsiesta\ the effective Hamiltonian sees a
  bulk environment. To ensure that \siesta\ also exhibits a bulk
  environment on the electrodes we add \emph{buffer} atoms towards the
  vacuum region to screen off the electrode region. These
  \emph{buffer} atoms is thus a technicality that has no influence on
  the \tsiesta\ calculation but they are necessary to ensure the
  electrode bulk properties.

  The above discussion is even more important when doing $\Nelec$-electrode
  calculations.

  \note all lines are additive for the buffer atoms and the input
  method is similar to that of \fdf{Geometry!Constraints} for the
  \fdf*{atom} line(s).

  \begin{fdfexample}
    %block TS.Atoms.Buffer
       atom [ 1 -- 5 ]
    %endblock
    # Or equivalently as a list
    TS.Atoms.Buffer [1 -- 5]
  \end{fdfexample}
  will remove atoms [1--5] from the calculation.

\end{fdfentry}

\begin{fdfentry}{TS!ElectronicTemperature}[energy]<\fdfvalue{ElectronicTemperature}>

  Define the temperature used for the Fermi distributions for the
  chemical potentials.
  %
  See \fdf{TS!ChemPot.<>!ElectronicTemperature}.

\end{fdfentry}

\begin{fdfentry}{TS!SCF!DM.Tolerance}[real]<\fdfvalue{SCF.DM!Tolerance}>%
  \fdfdepend{SCF.DM!Tolerance,SCF.DM!Converge}

  The density matrix tolerance for the \tsiesta\ SCF cycle.

\end{fdfentry}

\begin{fdfentry}{TS!SCF!H.Tolerance}[energy]<\fdfvalue{SCF.H!Tolerance}>%
  \fdfdepend{SCF.H!Tolerance,SCF.H!Converge}

  The Hamiltonian tolerance for the \tsiesta\ SCF cycle.

\end{fdfentry}

\begin{fdflogicalT}{TS!SCF!dQ.Converge}

  Whether \tsiesta\ should check whether the total charge is within a
  provided tolerance, see \fdf{TS!SCF!dQ.Tolerance}.

\end{fdflogicalT}

\begin{fdfentry}{TS!SCF!dQ.Tolerance}[real]<$\mathrm{Q(device)}\cdot 10^{-3}$>%
  \fdfdepend{TS!SCF!dQ.Converge}

  The charge tolerance during the SCF.

  The charge is not stable in \tsiesta\ calculations and this flag
  ensures that one does not, by accident, do post-processing of files
  where the charge distribution is completely wrong.

  A too high tolerance may heavily influence the electrostatics of the
  simulation.

  \note Please see \fdf{TS!dQ} for ways to reduce charge loss in
  equilibrium calculations.

\end{fdfentry}

\begin{fdfentry}{TS!SCF.Initialize}[string]<diagon|transiesta>%

  Control which initial guess should be used for \tsiesta. The general
  way is the \fdf*{diagon} solution method (which is preferred),
  however, one can start a \tsiesta\ run immediately. If you start
  directly with \tsiesta\ please refer to these flags:
  \fdf{TS!Elecs!DM.Init} and \fdf{TS!Fermi.Initial}.

  \note Setting this to \fdf*{transiesta} is highly experimental and
  convergence may be extremely poor.

\end{fdfentry}

\begin{fdfentry}{TS!Fermi.Initial}[energy]<$\sum^{N_E}_iE_F^i/N_E$>

  Manually set the initial Fermi level to a predefined value.

  \note this may also be used to change the Fermi level for
  calculations where you restart calculations. Using this feature is
  highly experimental.

\end{fdfentry}

\begin{fdfentry}{TS!Weight.Method}[string]<orb-orb|[[un]correlated+][sum|tr]-atom-[atom|orb]|mean>

  Control how the NEGF weighting scheme is conducted. Generally one
  should only use the \fdf*{orb-orb} while the others are present for
  more advanced usage. They refer to how the weighting coefficients of
  the different non-equilibrium contours are performed. In the
  following the weight are denoted in a two-electrode setup while they
  are generalized for multiple electrodes.

  \def\mypropto{\,\oppropto^{||}\,} %
  \def\mn{{\mu\nu}} %
  Define the normalised geometric mean as $\mypropto$ via
  \begin{equation}
    w\mypropto \langle\cdot^L\rangle\equiv
    \frac{\langle\cdot^L\rangle}{\langle\cdot^L\rangle+\langle\cdot^R\rangle}.
  \end{equation}

  When applying a bias, \tsiesta\ will printout the following during
  the SCF cycle:
\begin{output}[fontsize=\footnotesize]
ts-err-D: ij(  447,   447), M =  1.8275, ew = -.257E-2, em = 0.258E-2. avg_em = 0.542E-06
ts-err-E: ij(  447,   447), M = -6.7845, ew = 0.438E-3, em = -.439E-3. avg_em = -.981E-07
ts-w-q:               qP1       qP2
ts-w-q:           219.150   216.997
ts-q:         D        E1        C1        E2        C2        dQ
ts-q:   436.147   392.146     3.871   392.146     3.871  7.996E-3
  \end{output}
  %
  The extra output corresponds to fine details in the integration
  scheme.
  \begin{description}[labelindent=3em, leftmargin=4.5em]
    \itemsep 10pt
    \parsep 0pt

    \item[\texttt{ts-err-*}] are estimated error outputs from the
    different integrals, for the density matrix (\texttt{D}) and the
    energy density matrix (\texttt{E}), see Eq.~(12) in
    \cite{Papior2017}. All values (except \texttt{avg\_em}) are for
    the given orbital site

    \begin{description}
      \itemsep 4pt
      \parsep 0pt

      \item[\texttt{ij(A,B)}] refers to the matrix element between orbital
      \texttt{A} and \texttt{B}

      \item[\texttt{M}] is the weighted matrix element value,
      $\sum_{\elec}w_\elec\DM^\elec$

      \item[\texttt{ew}] is the maximum difference between
      $\sum_{\elec}w_\elec\DM^\elec-\DM^\elec$ for all $\elec$.

      \item[\texttt{em}] is the maximum difference between
      $\DM^{\elec'}-\DM^\elec$ for all combinations of $\elec$ and
      $\elec'$.

      \item[\texttt{avg\_em}] is the averaged difference of \texttt{em} for all
      orbital sites.

    \end{description}

    \item[\texttt{ts-w-q}] is the Mulliken charge from the different
    integrals: $\Tr[w_\elec\DM^\elec\SO]$

  \end{description}

  \begin{fdfoptions}

    \option[orb-orb]%
    \fdfindex*{TS!Weight.Method:orb-orb}%
    Weight each orbital-density matrix element individually.

    \option[tr-atom-atom]%
    \fdfindex*{TS!Weight.Method:tr-atom-atom}%
    Weight according to the trace of the atomic density matrix sub-blocks
    \begin{equation}
      w_{ij}^{\Tr} \mypropto
      \sqrt{
          % First the i'th atom
          \sum_{\in\{i\}}(\Delta\rho_{\mu\mu}^L)^2
          \; % ensure a little space between them
          % second the j'th atom
          \sum_{\in\{j\}}(\Delta\rho_{\mu\mu}^L)^2
      }
    \end{equation}

    \option[tr-atom-orb]%
    \fdfindex*{TS!Weight.Method:tr-atom-orb}%

    Weight according to the trace of the atomic density matrix
    sub-block times the weight of the orbital weight
    \begin{equation}
      w_{ij,\mn}^{\Tr} \mypropto
      \sqrt{
          w_{ij}^{\Tr}
          w_{ij,\mn}
      }
    \end{equation}

    \option[sum-atom-atom]%
    \fdfindex*{TS!Weight.Method:sum-atom-atom}%

    Weight according to the total sum of the atomic density matrix
    sub-blocks
    \begin{equation}
      w_{ij,\mn}^{\Sigma} \mypropto
      \sqrt{
          % First the i'th atom
          \sum_{\in\{i\}}(\Delta\rho_{\mn}^L)^2
          \; % ensure a little space between them
          % second the j'th atom
          \sum_{\in\{j\}}(\Delta\rho_{\mn}^L)^2
      }
    \end{equation}

    \option[sum-atom-orb]%
    \fdfindex*{TS!Weight.Method:sum-atom-orb}%

    Weight according to the total sum of the atomic density matrix
    sub-block times the weight of the orbital weight
    \begin{equation}
      w_{ij,\mn}^{\Sigma} \mypropto
      \sqrt{
          w_{ij}^{\Sigma}
          w_{ij,\mn}
      }
    \end{equation}

    \option[mean]%
    \fdfindex*{TS!Weight.Method:mean}%

    A standard average.

  \end{fdfoptions}


  Each of the methods (except \fdf*{mean}) comes in a correlated and
  uncorrelated variant where $\sum$ is either outside or inside the
  square, respectively.

\end{fdfentry}

\begin{fdfentry}{TS!Weight.k.Method}[string]<correlated|uncorrelated>

  Control weighting \emph{per} $k$-point or the full sum. I.e. if
  \fdf*{uncorrelated} is used it will weight $n_k$ times if there are
  $n_k$ $k$-points in the Brillouin zone.

\end{fdfentry}

\begin{fdflogicalT}{TS!Forces}

  Control whether the forces are calculated. If \emph{not} \tsiesta\
  will use slightly less memory and the performance slightly
  increased, however the final forces shown are incorrect.

  If this is \fdftrue\ the file \sysfile{TSFA} (and possibly the
  \sysfile{TSFAC}) will be created. They contain forces for the atoms
  that are having updated density-matrix elements
  (\fdf{TS!Elec.<>!DM-update:all}).

  Generally one should not expect good forces close to the
  electrode/device interface since this typically has some
  electrostatic effects that are inherent to the \tsiesta\ method.
  Forces on atoms \emph{far} from the electrode can safely be
  analyzed.

\end{fdflogicalT}

\begin{fdfentry}{TS!dQ}[string]<none|buffer|fermi>
  \fdfindex*{TS!dQ:fermi}

  Any excess/deficiency of charge can be re-adjusted after each
  \tsiesta\ cycle to reduce charge fluctuations in the cell.

  \note recommended to \emph{only} use charge corrections for
  $0\,\mathrm{V}$ calculations.

  The non-neutral charge in \tsiesta\ cycles is an expression of one
  of the following things:
  \begin{enumerate}
    \item An incorrect screening towards the electrodes. To check
    this, simply add more electrode layers towards the device at each
    electrode and see how the charge evolves. It should tend to zero.

    The best way to check this is to follow these steps:
    \begin{enumerate}
      \item%
      Perform a \siesta-only calculation (the resulting DM
      should be used as the starting point for both following
      calculations)

      \item%
      Perform a \tsiesta\ calculation with the option
      \fdf{TS!Elecs!DM.Init:diagon} (please note that the electrode
      option has precedence, so remove any entry from the
      \fdf{TS!Elec.<>} block)

      \item%
      Perform a \tsiesta\ calculation with the option
      \fdf{TS!Elec.<>!DM-init:bulk} (please note that the electrode
      option has precedence, so remove any entry from the
      \fdf{TS!Elec.<>} block)

    \end{enumerate}

    Now compare the final output and the initial charge distribution,
    e.g.:
    \begin{output}
>>> TS.Elecs.DM.Init diagon
transiesta: Charge distribution, target =    396.00000
Total charge                  [Q]  :   396.00000

>>> TS.Elecs.DM.Init bulk
transiesta: Charge distribution, target =    396.00000
Total charge                  [Q]  :   395.9995
\end{output}

    The above shows that there is very little charge difference
    between the bulk electrode DM and the scattering region. This
    ensures that the charge distribution are similar and that your
    electrode is sufficiently screened.

    Additionally one may compare the final output such as total
    energies, calculated DOS and ADOS (see \tbtrans). If the two
    calculations show different properties, one should carefully
    examine the system setup.

    \item An incorrect reference energy level. In \tsiesta\ the Fermi
    level is calculated from the \siesta\ SCF. However, the \siesta\
    Fermi level corresponds to a periodic calculation and \emph{not}
    an open system calculation such as NEGF.

    If the first step shows a good screening towards the electrode it
    is usually the reference energy level, then use \fdf{TS!dQ:fermi}.

    \item A combination of the above, this is the typical case.
  \end{enumerate}

  \begin{fdfoptions}

    \option[none]%
    No charge corrections are introduced.

    \option[buffer]%
    Excess/missing electrons are placed in the buffer regions (buffer
    atoms are required to exist)

    \option[fermi] %
    Correct the charge filling by calculating a new reference energy
    level (referred to as the Fermi level). \\
    We approximate the contribution to be constant around the Fermi
    level and find
    \begin{equation}
      \label{eq:fermi-shift}
      \mathrm{d}E_F = \frac{Q'-Q}{Q|_{E_F}},
    \end{equation}
    where $Q'$ is the charge from a \tsiesta\ SCF step
    and $Q|_{E_F}$ is the equilibrium charge at the current Fermi
    level, $Q$ is the supposed charge to reside in the
    calculation. Fermi correction utilizes Eq.~\eqref{eq:fermi-shift} for
    the first correction and all subsequent corrections are based on a
    cubic spline interpolation to faster converge the
    ``correct'' Fermi level.

    This method will create a file called \file{TS\_FERMI}.

    \note correcting the reference energy level is a costly
    operation since the SCF cycle typically gets
    \emph{corrupted} resulting in many more SCF cycles.

  \end{fdfoptions}

\end{fdfentry}

\begin{fdfentry}{TS!dQ!Factor}[real]<0.8>

  Any positive value close to $1$. $0$ means no charge correction. $1$
  means total charge correction. This will reduce the fluctuations in
  the SCF and setting this to $1$ may result in difficulties in
  converging.

\end{fdfentry}

\begin{fdfentry}{TS!dQ!Fermi.Tolerance}[real]<0.01>

  The tolerance at which the charge correction will converge. Any
  excess/missing charge ($|Q'-Q|>\mathrm{Tol}$) will result in a
  correction for the Fermi level.

\end{fdfentry}

\begin{fdfentry}{TS!dQ!Fermi.Max}[energy]<$1.5\,\mathrm{eV}$>%

  The maximally allowed value that the Fermi level will change from a
  charge correction using the Fermi correction method. In case the
  Fermi level lies in between two bands a DOS of $0$ at the Fermi
  level will make the Fermi change equal to $\infty$. This is not
  physical and the user can thus truncate the correction.

  \note If you know the band-gab, setting this to $1/4$ (or smaller)
  of the band gab seems like a better value than the rather
  arbitrarily default one.

\end{fdfentry}

\begin{fdfentry}{TS!dQ!Fermi.Eta}[energy]<$1\,\mathrm{meV}$>%

  The $\eta$ value that we extrapolate the charge at the poles to.
  Usually a smaller $\eta$ value will mean larger changes in the
  Fermi level. If the charge convergence w.r.t. the Fermi level is
  fluctuating a lot one should increase this $\eta$ value.

\end{fdfentry}

\begin{fdflogicalT}{TS!HS.Save}
  \fdfindex*{TS!HS.Save:true}

  Must be \fdftrue\ for saving the Hamiltonian (\sysfile{TSHS}). Can only be set if
  \fdf{SolutionMethod} is not \fdf*{transiesta}.

  The default is \fdffalse\ for \fdf{SolutionMethod} different from
  \fdf*{transiesta} and if \code{--electrode} has not been passed as a
  command line argument.

\end{fdflogicalT}

\begin{fdflogicalT}{TS!DE.Save}
  \fdfindex*{TS!DE.Save:true}

  Must be \fdftrue\ for saving the density and energy density matrix
  for continuation runs (\sysfile{TSDE}). Can only be set if
  \fdf{SolutionMethod} is not \fdf*{transiesta}.

  The default is \fdffalse\ for \fdf{SolutionMethod} different from
  \fdf*{transiesta} and if \code{--electrode} has not been passed as a
  command line argument.

\end{fdflogicalT}

\begin{fdflogicalF}{TS!S.Save}

  This is a flag mainly used for the Inelastica code to produce
  overlap matrices for Pulay corrections. This should only be used by
  advanced users.

\end{fdflogicalF}


\begin{fdflogicalF}{TS!SIESTA.Only}

  Stop \tsiesta\ right after the initial diagonalization run in
  \siesta. Upon exit it will also create the \sysfile{TSDE} file which
  may be used for initialization runs later.

  This may be used to start several calculations from the same initial
  density matrix, and it may also be used to rescale the Fermi level
  of electrodes. The rescaling is primarily used for semi-conductors
  where the Fermi levels of the device and electrodes may be
  misaligned.

\end{fdflogicalF}


\begin{fdflogicalF}{TS!Analyze}

  When using the BTD solution method (\fdf{TS!SolutionMethod}) this
  will analyze the Hamiltonian and printout an analysis of the
  sparsity pattern for optimal choice of the BTD partitioning
  algorithm.

  This yields information regarding the \fdf{TS!BTD!Pivot} flag.

  \note we advice users to \emph{always} run an analyzation step prior
  to actual calculation and select the \emph{best} BTD format. This
  analyzing step is very fast and may be performed on small
  work-station computers, even on systems of $\gg10,000$ orbitals.

  To run the analyzing step you may do:
  \begin{shellexample}
    siesta -fdf TS.Analyze RUN.fdf > analyze.out
  \end{shellexample}
  note that there is little gain on using MPI and it should complete
  within a few minutes, no matter the number of orbitals.

  Choosing the best one may be difficult. Generally one should choose
  the pivoting scheme that uses the least amount of memory. However,
  one should also choose the method with the largest block-size being
  as small as possible. As an example:
  \begin{output}[fontsize=\footnotesize]
TS.BTD.Pivot atom+GPS
...
    BTD partitions (7):
     [ 2984, 2776, 192, 192, 1639, 4050, 105 ]
    BTD matrix block size [max] / [average]: 4050 /   1705.429
    BTD matrix elements in % of full matrix:   47.88707 %

TS.BTD.Pivot atom+GGPS
...
    BTD partitions (6):
     [ 2880, 2916, 174, 174, 2884, 2910 ]
    BTD matrix block size [max] / [average]: 2916 /   1989.667
    BTD matrix elements in % of full matrix:   48.62867 %

  \end{output}
  Although the GPS method uses the least amount of memory, the GGPS
  will likely perform better as the largest block in GPS is $4050$
  vs. $2916$ for the GGPS method.

\end{fdflogicalF}

\begin{fdflogicalF}{TS!Analyze.Graphviz}
  \fdfdepend{TS!Analyze}

  If performing the analysis, also create the connectivity graph and
  store it as \file{GRAPHVIZ\_atom.gv} or \file{GRAPHVIZ\_orbital.gv}
  to be post-processed in Graphviz\footnote{\url{www.graphviz.org}}.

\end{fdflogicalF}
