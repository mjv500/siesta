\textit{This Reference Manual contains descriptions of all the input,
  output and execution features of \siesta, but is not really a
  tutorial introduction to the program. Interested users can find
  tutorial material prepared for \siesta\ schools and workshops at
  the web page} \url{https://docs.siesta-project.org}


\siesta\index{Siesta@\siesta} (Spanish Initiative for
Electronic Simulations with
Thousands of Atoms) is both a method and its computer program implementation,
to perform electronic structure calculations and \textit{ab initio} molecular
dynamics simulations of molecules and solids. Its main characteristics are:
\begin{itemize}
\item
It uses the standard Kohn-Sham selfconsistent density functional
method in the local density (LDA-LSD) and generalized gradient (GGA)
approximations, as well as in a non local functional that includes
van der Waals interactions (VDW-DF).
\item
It uses norm-conserving pseudopotentials in their fully nonlocal
(Kleinman-Bylander) form.
\item
It uses atomic orbitals as a basis set, allowing unlimited multiple-zeta
and angular momenta, polarization and off-site orbitals. The radial
shape of every orbital is numerical and any shape can be used and provided
by the user, with the only condition that it has to be of finite support,
i.e., it has to be strictly zero beyond a user-provided distance from the
corresponding nucleus.
Finite-support basis sets are the key for calculating the Hamiltonian
and overlap matrices in $O(N)$ operations.
\item
Projects the electron wavefunctions and density onto a real-space
grid in order to calculate the Hartree and exchange-correlation
potentials and their matrix elements.
\item
Besides the standard Rayleigh-Ritz eigenstate method, it allows
the use of localized linear combinations of the occupied orbitals
(valence-bond or Wannier-like functions), making the computer
time and memory scale linearly with the number of atoms.
Simulations with several hundred atoms are feasible with
modest workstations.
\item
It is written in Fortran 2003 and memory is allocated dynamically.
\item
It may be compiled for serial or parallel execution (under MPI).

\end{itemize}

It routinely provides:
\begin{itemize}
  \item Total and partial energies.
  \item Atomic forces.
  \item Stress tensor.
  \item Electric dipole moment.
  \item Atomic, orbital and bond populations (Mulliken).
  \item Electron density.
\end{itemize}

And also (though not all options are compatible):
\begin{itemize}
  \item Geometry relaxation, fixed or variable cell.
  \item Constant-temperature molecular dynamics (Nose thermostat).
  \item Variable cell dynamics (Parrinello-Rahman).
  \item Spin polarized calculations (colinear or not).
  \item k-sampling of the Brillouin zone.
  \item Local and orbital-projected density of states.
  \item COOP and COHP curves for chemical bonding analysis.
  \item Dielectric polarization.
  \item Vibrations (phonons).
  \item Band structure.
  \item Ballistic electron transport under non-equilibrium (through \tsiesta)
\end{itemize}


Starting from version 3.0, \siesta\ includes the \tsiesta\index{TranSIESTA@\tsiesta}
module. \tsiesta\ provides the ability to model open-boundary systems where ballistic
electron transport is taking place.  Using \tsiesta\ one can compute electronic
transport properties, such as the zero bias conductance and the I-V characteristic, of a
nanoscale system in contact with two electrodes at different electrochemical potentials.
The method is based on using non equilibrium Greens functions (NEGF), that are
constructed using the density functional theory Hamiltonian obtained from a given electron
density. A new density is computed using the NEGF formalism, which closes the DFT-NEGF
self consistent cycle.

Starting from version 4.1, \tsiesta\ is an intrinsic part of the
\siesta\ code. I.e. a separate executable is not necessary
anymore. See Sec.~\ref{sec:transiesta} for details.

For more details on the formalism, see the main \tsiesta\
reference cited below. A section has been added to this User's Guide,
that describes the necessary steps involved in doing transport
calculations, together with the currently implemented input options.

\vspace{0.5cm}
{\large \textbf{References:} }

\begin{itemize}

\item
``Unconstrained minimization approach for electronic computations
that scales linearly with system size''
P. Ordej\'on, D. A. Drabold, M. P. Grumbach and R. M. Martin,
Phys. Rev. B \textbf{48}, 14646 (1993);
``Linear system-size methods for electronic-structure calculations''
Phys. Rev. B \textbf{51} 1456 (1995), and references therein.

Description of the order-\textit{N} eigensolvers
implemented in this code.

\item
``Self-consistent order-$N$ density-functional
calculations for very large systems''
P. Ordej\'on, E. Artacho and J. M. Soler,
Phys. Rev. B \textbf{53}, 10441, (1996).

Description of a previous version of this methodology.

\item
``Density functional method for very large systems with LCAO basis sets''
D. S\'anchez-Portal, P. Ordej\'on, E. Artacho and J. M. Soler,
Int. J. Quantum Chem., \textbf{65}, 453 (1997).

Description of the present method and code.

\item
``Linear-scaling ab-initio calculations for large and complex systems''
E. Artacho, D. S\'anchez-Portal, P. Ordej\'on, A. Garc\'{\i}a and
J. M. Soler, Phys. Stat. Sol. (b) \textbf{215}, 809 (1999).

Description of the numerical atomic orbitals (NAOs) most commonly
used in the code, and brief review of applications as of March 1999.

\item
``Numerical atomic orbitals for linear-scaling calculations''
J. Junquera, O. Paz, D. S\'anchez-Portal, and E. Artacho, Phys. Rev. B
 \textbf{64}, 235111, (2001).

Improved, soft-confined NAOs.

\item
``The \siesta\ method for ab initio order-$N$ materials simulation''
J. M. Soler, E. Artacho, J.D. Gale, A. Garc\'{\i}a, J. Junquera, P. Ordej\'on,
and D. S\'anchez-Portal, J. Phys.: Condens. Matter \textbf{14}, 2745-2779 (2002)

Extensive description of the \siesta\ method.

\item
``Computing the properties of materials from first principles
with  \siesta'', D. S\'anchez-Portal, P. Ordej\'on,
and E. Canadell, Structure and Bonding \textbf{113},
103-170 (2004).

Extensive review of applications as of summer 2003.

\item
 ``Improvements on non-equilibrium and transport Green function techniques: The next-generation TranSIESTA'',
 Nick Papior, Nicolas Lorente, Thomas Frederiksen, Alberto Garc\'{\i}a and
 Mads Brandbyge, Computer Physics Communications, \textbf{212}, 8--24 (2017).

 Description of the \tsiesta\ method.

\item
 ``Density-functional method for nonequilibrium electron transport'',
 Mads Brandbyge, Jose-Luis Mozos, Pablo Ordej\'on, Jeremy Taylor,
 and Kurt Stokbro, Phys. Rev. B \textbf{65}, 165401 (2002).

 Description of the original \tsiesta\ method (prior to 4.1).

 \item
 ``Siesta: Recent developments and applications'',
 Alberto Garc\'{\i}a, \emph{et al.}, J. Chem. Phys. \textbf{152}, 204108
 (2020).

 Extensive review of applications and developments as of 2020.

\end{itemize}

For more information you can visit the web page
\url{https://siesta-project.org}.