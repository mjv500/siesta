There are many ways to give \siesta\ structural information.

\begin{itemize}
  \item%
  Directly from the fdf file in traditional format.

  \item%
  Directly from the fdf file in the newer Z-Matrix format, using
  a \fdf{Zmatrix} block.

  \item%
  From an external data file
\end{itemize}

Note that, regardless of the way in which the structure is described,
the \fdf{ChemicalSpeciesLabel} block is mandatory.

In the following sections we document the different structure input
methods, and provide a guide to their precedence.

\subsubsection{Traditional structure input in the fdf file}

Firstly, the size of the cell itself should be specified, using some
combination of the options \fdf{LatticeConstant},
\fdf{LatticeParameters}, and \fdf{LatticeVectors}, and
\fdf{SuperCell}.  If nothing is specified, \siesta\ will construct a
cubic cell in which the atoms will reside as a cluster (a molecule).

Secondly, the positions of the atoms within the cells must be
specified, using either the traditional \siesta\ input format (a
modified xyz format) which must be described within a
\fdf{AtomicCoordinatesAndAtomicSpecies} block.

\begin{fdfentry}{LatticeConstant}[length]
  \fdfdepend{LatticeParameters,LatticeVectors}

  Lattice constant. This is just to define the scale of the lattice
  vectors.

  \note This defaults to $1\,\mathrm{Ang}$ when used in combination
  with \fdf{LatticeParameters} or \fdf{LatticeVectors}. Otherwise it
  is not used.

\end{fdfentry}

\begin{fdfentry}{LatticeParameters}[block]
  \fdfdepend{LatticeConstant}

  Crystallographic way of specifying the lattice vectors, by giving
  six real numbers: the three vector modules, $a$, $b$, and $c$, and
  the three angles $\alpha$ (angle between $\vec b$ and $\vec c$),
  $\beta$, and $\gamma$. The three modules are in units of
  \fdf{LatticeConstant}, the three angles are in degrees.

  For example a square cell with side-lengths equal to \fdf{LatticeConstant}.
  \begin{fdfexample}
    1.0  1.0  1.0   90.  90.  90.
  \end{fdfexample}

\end{fdfentry}

\begin{fdfentry}{LatticeVectors}[block]
  \fdfdepend{LatticeConstant}

  The cell vectors are read in units of the lattice constant,
  \fdf{LatticeConstant} which defaults to $1\,\mathrm{Ang}$.

  They are read as a matrix with each vector being one line.

  For example a square cell with side-lengths equal to \fdf{LatticeConstant}.
  \begin{fdfexample}
    1.0  0.0  0.0
    0.0  1.0  0.0
    0.0  0.0  1.0
  \end{fdfexample}

\end{fdfentry}

\begin{fdfentry}{SuperCell}[block]

  Integer 3x3 matrix defining a supercell in terms of the unit cell.
  Any values larger than $1$ will expand the unitcell (plus atoms)
  along that lattice vector direction (if possible).
  \begin{fdfexample}
     %block SuperCell
        M(1,1)  M(2,1)  M(3,1)
        M(1,2)  M(2,2)  M(3,2)
        M(1,3)  M(2,3)  M(3,3)
     %endblock SuperCell
  \end{fdfexample}
  and the supercell is defined as
  $\mathrm{SuperCell}(ix,i) = \sum_j \mathrm{CELL}(ix,j)*M(j,i)$.
  Notice that the matrix indexes are inverted: each input line
  specifies one supercell vector.

  \textit{Warning:} \fdf{SuperCell} is disregarded if the geometry is
  read from the XV file, which can happen inadvertently.

  \textit{Use:} The atomic positions must be given only for the unit
  cell, and they are 'cloned' automatically in the rest of the
  supercell. The \fdf{NumberOfAtoms} given must also be that in a
  single unit cell. However, all values in the output are given for
  the entire supercell. In fact, \texttt{CELL} is immediately
  redefined as the whole supercell and the program no longer knows the
  existence of an underlying unit cell.  All other input (apart from
  NumberOfAtoms and atomic positions), including
  \fdf{kgrid!MonkhorstPack} must refer to the supercell (this is a
  change over previous versions). Therefore, to avoid confusions, we
  recommend to use \fdf{SuperCell} only to generate atomic positions,
  and then to copy them from the output to a new input file with all
  the atoms specified explicitly and with the supercell given as a
  normal unit cell.

\end{fdfentry}

\begin{fdfentry}{AtomicCoordinatesFormat}[string]<Bohr>

  Character string to specify the format of the atomic positions in
  input. These can be expressed in four forms:

  \begin{fdfoptions}
    \option[Bohr|NotScaledCartesianBohr]%
    \fdfindex*{AtomicCoordinatesFormat:Bohr}%
    \fdfindex*{AtomicCoordinatesFormat:NotScaledCartesianBohr}%

    atomic positions are given directly in Bohr, in Cartesian
    coordinates

    \option[Ang|NotScaledCartesianAng]%
    \fdfindex*{AtomicCoordinatesFormat:Ang}%
    \fdfindex*{AtomicCoordinatesFormat:NotScaledCartesianAng}%

    atomic positions are given directly in \AA ngstr\"om, in Cartesian
    coordinates

    \option[LatticeConstant|ScaledCartesian]%
    \fdfindex*{AtomicCoordinatesFormat:ScaledCartesian}%
    \fdfindex*{AtomicCoordinatesFormat:LatticeConstant}%

    atomic positions are given in Cartesian coordinates, in units of
    the lattice constant

    \option[Fractional|ScaledByLatticeVectors]%
    \fdfindex*{AtomicCoordinatesFormat:Fractional}%
    \fdfindex*{AtomicCoordinatesFormat:ScaledByLatticeVectors}%

    atomic positions are given referred to the lattice vectors

  \end{fdfoptions}

\end{fdfentry}


\begin{fdfentry}{AtomCoorFormatOut}[string]<\fdfvalue{AtomicCoordinatesFormat}>

  Character string to specify the format of the atomic positions in
  output.

  Same possibilities as for input \fdf{AtomicCoordinatesFormat}.

\end{fdfentry}

\begin{fdfentry}{AtomicCoordinatesOrigin}[block/string]

  The user can request a rigid shift of the coordinates, for example
  to place a molecule near the center of the cell. This shift can be
  specified in two ways:

  \begin{itemize}
    \item By an explicit vector,
  given in the same format and units as the coordinates. Notice that the atomic
  positions (shifted or not) need not be within the cell formed by
  \fdf{LatticeVectors}, since periodic boundary conditions are
  always assumed.

  This defaults to the origin:
  \begin{fdfexample}
    0.0   0.0   0.0
  \end{fdfexample}

    \item By a string that indicates an automatic shift that places
      the ``center'' of the system at the center of the unit cell, or
      that places the system near the borders of the
      cell. In this case, the contents of the block, or the values
      associated directly to the label (see below) can be:

  \begin{fdfoptions}
    \option[COP]%
    \fdfindex*{AtomicCoordinatesOrigin:COP}

    Place the center of coordinates in the middle of the unit-cell.

    \option[COM]%
    \fdfindex*{AtomicCoordinatesOrigin:COM}

    Place the center of mass in the middle of the unit-cell.

    \option[MIN]%
    \fdfindex*{AtomicCoordinatesOrigin:MIN}

    Shift the coordinates so that the minimum value along each
    cartesian axis is $0$.

  \end{fdfoptions}

   \note  Ghost atoms are not taken into account for the above ``centering''
    calculations (but their coordinates are indeed shifted).

  All string options may be given an optional value. For instance,
  \fdf*{COP-XZ} which limits the \fdf*{COP} option to only affect $x$
  and $z$ Cartesian coordinates.

  The accepted suffixes are: \fdf*{-X}, \fdf*{-Y}, \fdf*{-Z},
  \fdf*{-XY}/\fdf*{-YX}, \fdf*{-YZ}/\fdf*{-YZ}, \fdf*{-XZ}/\fdf*{-ZX}
  and anything else will be regarded as all directions.

  \begin{fdfexample}
    AtomicCoordinatesOrigin COP-X ! COP only for x-direction
    AtomicCoordinatesOrigin COM-ZY ! COM only for y- and z-directions
    AtomicCoordinatesOrigin MIN-Z ! MIN only for z-direction
    AtomicCoordinatesOrigin MIN-XYZ ! MIN for all directions
    AtomicCoordinatesOrigin MIN ! MIN for all directions
  \end{fdfexample}

  \end{itemize}

\end{fdfentry}

\begin{fdfentry}{AtomicCoordinatesAndAtomicSpecies}[block]

  Block specifying the position and species of each atom.  One line
  per atom, the reading is done this way:
  \begin{shellexample}
       From ia = 1 to natoms
            read: xa(ix,ia), isa(ia)
  \end{shellexample}
  where \shell{xa(ix,ia)} is the \shell{ix} coordinate of atom
  \shell{iai} in the format (units) specified by
  \fdf{AtomicCoordinatesFormat}, and \shell{isa(ia)} is the species
  index of atom \shell{ia}.

  \note This block \emph{must} be present in the fdf file. If
  \fdf{NumberOfAtoms} is not specified, \fdf{NumberOfAtoms} will be
  defaulted to the number of atoms in this block.

  \note \fdf{Zmatrix} has precedence if specified.

\end{fdfentry}


\subsubsection{Z-matrix format and constraints}
\label{sec:Zmatrix}

The advantage of the traditional format is that it is
much easier to set up a system. However, when working
on systems with constraints, there are only a limited
number of (very simple) constraints that may be expressed
within this format, and recompilation is needed for each
new constraint.

For any more involved set of constraints, a
full \fdf{Zmatrix} formulation should be used - this
offers much more control, and may be specified fully at
run time (thus not requiring recompilation) - but
it is more work to generate the input files for this form.


\begin{fdfentry}{Zmatrix}[block]

  This block provides a means for inputting the system geometry using
  a Z-matrix format, as well as controlling the optimization
  variables. This is particularly useful when working with molecular
  systems or restricted optimizations (such as locating transition
  states or rigid unit movements). The format also allows for hybrid
  use of Z-matrices and Cartesian or fractional blocks, as is
  convenient for the study of a molecule on a surface.  As is always
  the case for a Z-matrix, the responsibility falls to the user to
  chose a sensible relationship between the variables to avoid triads
  of atoms that become linear.

  Below is an example of a Z-matrix input for a water molecule:
  \begin{fdfexample}
    %block Zmatrix
    molecule fractional
      1 0 0 0   0.0 0.0 0.0 0 0 0
      2 1 0 0   HO1 90.0 37.743919 1 0 0
      2 1 2 0   HO2 HOH 90.0 1 1 0
    variables
        HO1 0.956997
        HO2 0.956997
        HOH 104.4
    %endblock Zmatrix
  \end{fdfexample}

  The sections that can be used within the Zmatrix block are as
  follows:

  Firstly, all atomic positions must be specified within either a
  ``\texttt{molecule}'' block or a ``\texttt{cartesian}'' block.  Any
  atoms subject to constraints more complicated than ``do not change
  this coordinate of this atom'' must be specified within a
  ``\texttt{molecule}'' block.

  \begin{fdfoptions}

    \option[molecule]%
    There must be one of these blocks for each independent set of
    constrained atoms within the simulation.

    This specifies the atoms that make up each molecule and their
    geometry. In addition, an option of ``\texttt{fractional}'' or
    ``\texttt{scaled}'' may be passed, which indicates that distances are
    specified in scaled or fractional units. In the absence of such an
    option, the distance units are taken to be the value of
    ``\texttt{ZM.UnitsLength}''.

    A line is needed for each atom in the molecule; the format of each
    line should be:
    \begin{fdfexample}
      Nspecies i j k r a t ifr ifa ift
    \end{fdfexample}

    Here the values \texttt{Nspecies}, \texttt{i}, \texttt{j}, \texttt{k},
    \texttt{ifr}, \texttt{ifa}, and \texttt{ift} are integers and
    \texttt{r}, \texttt{a}, and \texttt{t} are double precision reals.

    For most atoms, \texttt{Nspecies} is the species number of the atom,
    \texttt{r} is distance to atom number \texttt{i}, \texttt{a} is the
    angle made by the present atom with atoms \texttt{j} and \texttt{i},
    while \texttt{t} is the torsional angle made by the present atom with
    atoms \texttt{k}, \texttt{j}, and \texttt{i}. The values \texttt{ifr},
    \texttt{ifa} and \texttt{ift} are integer flags that indicate whether
    \texttt{r}, \texttt{a}, and \texttt{t}, respectively, should be
    varied; 0 for fixed, 1 for varying.


    The first three atoms in a molecule are a special case. Because there
    are insufficient atoms defined to specify a distance/angle/torsion,
    the values are set differently. For atom 1, \texttt{r}, \texttt{a},
    and \texttt{t}, are the Cartesian coordinates of the atom.  For the
    second atom, \texttt{r}, \texttt{a}, and \texttt{t} are the
    coordinates in spherical form of the second atom relative to the
    first: first the radius, then the polar angle (angle between the
    $z$-axis and the displacement vector) and then the azimuthal angle
    (angle between the $x$-axis and the projection of the displacement
    vector on the $x$-$y$ plane). Finally, for the third atom, the numbers
    take their normal form, but the torsional angle is defined relative to
    a notional atom 1 unit in the z-direction above the atom \texttt{j}.

    Secondly. blocks of atoms all of which are subject to the simplest of
    constraints may be specified in one of the following three ways,
    according to the units used to specify their coordinates:

    \option[cartesian]%
    This section specifies a block of atoms
    whose coordinates are to be specified in Cartesian coordinates. Again,
    an option of ``\texttt{fractional}'' or ``\texttt{scaled}'' may be
    added, to specify the units used; and again, in their absence, the
    value of ``\texttt{ZM.UnitsLength}'' is taken.

    The format of each atom in the block will look like:
    \begin{fdfexample}
      Nspecies x y z ix iy iz
    \end{fdfexample}

    Here \texttt{Nspecies}, \texttt{ix}, \texttt{iy}, and \texttt{iz} are
    integers and \texttt{x}, \texttt{y}, \texttt{z} are
    reals. \texttt{Nspecies} is the species number of the atom being
    specified, while \texttt{x}, \texttt{y}, and \texttt{z} are the
    Cartesian coordinates of the atom in whichever units are being
    used. The values \texttt{ix}, \texttt{iy} and \texttt{iz} are integer
    flags that indicate whether the \texttt{x}, \texttt{y}, and \texttt{z}
    coordinates, respectively, should be varied or not. A value of 0
    implies that the coordinate is fixed, while 1 implies that it should
    be varied.  \textbf{NOTE}: When performing ``variable cell''
    optimization while using a Zmatrix format for input, the algorithm
    will not work if some of the coordinates of an atom in a
    \texttt{cartesian} block are variables and others are not (i.e.,
    \texttt{ix iy iz} above must all be 0 or 1). This will be fixed in
    future versions of the program.

    A Zmatrix block may also contain the following, additional, sections, which
    are designed to make it easier to read.


    \option[constants]%
    Instead of specifying a numerical value, it is possible to specify
    a symbol within the above geometry definitions. This section
    allows the user to define the value of the symbol as a
    constant. The format is just a symbol followed by the value:
    \begin{fdfexample}
      HOH 104.4
    \end{fdfexample}


    \option[variables]%
    Instead of specifying a numerical value, it is possible to specify
    a symbol within the above geometry definitions. This section
    allows the user to define the value of the symbol as a
    variable. The format is just a symbol followed by the value:
    \begin{fdfexample}
      HO1 0.956997
    \end{fdfexample}

    Finally, constraints must be specified in a \fdf*{constraints} block.


    \option[constraint]%
    This sub-section allows the user to create
    constraints between symbols used in a Z-matrix:
    \begin{fdfexample}
      constraint
        var1 var2 A B
    \end{fdfexample}
    Here \fdf*{var1} and \fdf*{var2} are text symbols for two
    quantities in the Z-matrix definition, and $A and $B are real
    numbers. The variables are related by $\fdf*{var1} = A*\fdf*{var2}
    + B$.

  \end{fdfoptions}

  An example of a Z-matrix input for a benzene molecule over a metal surface is:
  \begin{fdfexample}
    %block Zmatrix
      molecule
       2 0 0 0 xm1 ym1 zm1 0 0 0
       2 1 0 0 CC 90.0 60.0 0 0 0
       2 2 1 0 CC CCC 90.0 0 0 0
       2 3 2 1 CC CCC 0.0 0 0 0
       2 4 3 2 CC CCC 0.0 0 0 0
       2 5 4 3 CC CCC 0.0 0 0 0
       1 1 2 3 CH CCH 180.0 0 0 0
       1 2 1 7 CH CCH 0.0 0 0 0
       1 3 2 8 CH CCH 0.0 0 0 0
       1 4 3 9 CH CCH 0.0 0 0 0
       1 5 4 10 CH CCH 0.0 0 0 0
       1 6 5 11 CH CCH 0.0 0 0 0
      fractional
       3 0.000000 0.000000 0.000000 0 0 0
       3 0.333333 0.000000 0.000000 0 0 0
       3 0.666666 0.000000 0.000000 0 0 0
       3 0.000000 0.500000 0.000000 0 0 0
       3 0.333333 0.500000 0.000000 0 0 0
       3 0.666666 0.500000 0.000000 0 0 0
       3 0.166667 0.250000 0.050000 0 0 0
       3 0.500000 0.250000 0.050000 0 0 0
       3 0.833333 0.250000 0.050000 0 0 0
       3 0.166667 0.750000 0.050000 0 0 0
       3 0.500000 0.750000 0.050000 0 0 0
       3 0.833333 0.750000 0.050000 0 0 0
       3 0.000000 0.000000 0.100000 0 0 0
       3 0.333333 0.000000 0.100000 0 0 0
       3 0.666666 0.000000 0.100000 0 0 0
       3 0.000000 0.500000 0.100000 0 0 0
       3 0.333333 0.500000 0.100000 0 0 0
       3 0.666666 0.500000 0.100000 0 0 0
       3 0.166667 0.250000 0.150000 0 0 0
       3 0.500000 0.250000 0.150000 0 0 0
       3 0.833333 0.250000 0.150000 0 0 0
       3 0.166667 0.750000 0.150000 0 0 0
       3 0.500000 0.750000 0.150000 0 0 0
       3 0.833333 0.750000 0.150000 0 0 0
     constants
       ym1 3.68
     variables
       zm1 6.9032294
       CC 1.417
       CH 1.112
       CCH 120.0
       CCC 120.0
     constraints
       xm1 CC -1.0 3.903229
   %endblock Zmatrix
  \end{fdfexample}

  Here the species 1, 2 and 3 represent H, C, and the metal of the
  surface, respectively.

  (Note: the above example shows the usefulness of symbolic names
  for the relevant coordinates, in particular for those which are
  allowed to vary. The current output options for Zmatrix information
  work best when this approach is taken. By using a ``fixed'' symbolic
  Zmatrix block and specifying the actual coordinates in a ``variables''
  section, one can monitor the progress of the optimization and
  easily reconstruct the coordinates of intermediate steps in the
  original format.)

\end{fdfentry}

\begin{fdfentry}{ZM!UnitsLength}[string]<Bohr>

  Parameter that specifies the units of length used during Z-matrix
  input.

  Specify \fdf*{Bohr} or \fdf*{Ang} for the corresponding unit of length.

\end{fdfentry}

\begin{fdfentry}{ZM!UnitsAngle}[string]<rad>

  Parameter that specifies the units of angles used during Z-matrix input.

  Specify \fdf*{rad} or \fdf*{deg} for the corresponding unit of angle.

\end{fdfentry}


\subsubsection{Output of structural information}

\siesta\ is able to generate several kinds of files containing
structural information (maybe too many).

\begin{itemize}

  \item\sysfile{STRUCT\_OUT}:%
  \siesta\ always produces a \sysfile*{STRUCT\_OUT} file with cell
  vectors in {\AA} and atomic positions in fractional
  coordinates. This file, renamed to \sysfile*{STRUCT\_IN} can be used
  for crystal-structure input.  Note that the geometry reported is the
  last one for which forces and stresses were computed.  See
  \fdf{UseStructFile}

  \item\sysfile{STRUCT\_NEXT\_ITER}:%
  This file is always written, in the same format as
  \sysfile*{STRUCT\_OUT} file. The only difference is that it contains
  the structural information \emph{after} it has been updated by the
  relaxation or the molecular-dynamics algorithms, and thus it could
  be used as input (renamed as \sysfile*{STRUCT\_IN}) for a
  continuation run, in the same way as the \sysfile*{XV} file.

  See \fdf{UseStructFile}

  \item\sysfile{XV}:%
  The coordinates are always written in the \sysfile*{XV} file, and
  overriden at every step.

  \item\file{OUT.UCELL.ZMATRIX}:%
  This file is produced if the Zmatrix format is being used for
  input. (Please note that \fdf{SystemLabel} is not used as a
  prefix.)  It contains the structural information in fdf form, with
  blocks for unit-cell vectors and for Zmatrix coordinates. The
  Zmatrix block is in a ``canonical'' form with the following
  characteristics:

\begin{verbatim}
1. No symbolic variables or constants are used.
2. The position coordinates of the first atom in each molecule
   are absolute Cartesian coordinates.
3. Any coordinates in ``cartesian'' blocks are also absolute Cartesians.
4. There is no provision for output of constraints.
5. The units used are those initially specified by the user, and are
   noted also in fdf form.
\end{verbatim}

  Note that the geometry reported is the last one for which forces and
  stresses were computed.

  \item\file{NEXT\_ITER.UCELL.ZMATRIX}:%
  A file with the same format as \file{OUT.UCELL.ZMATRIX} but with
  a possibly updated geometry.

  \item The coordinates can be also accumulated
  in the \sysfile{MD} or \sysfile{MDX} files
  depending on \fdf{WriteMDHistory}.

  \item Additionally, several optional formats are supported:
  \begin{fdflogicalF}{WriteCoorXmol}
    \index{JMol@\textsc{JMol}}
    \index{XMol@\textsc{XMol}}
    \index{Molden@\textsc{Molden}}

    If \fdftrue\ it originates the writing of an extra file named
    \sysfile{xyz} containing the final atomic coordinates in a format
    directly readable by \method{XMol}.\footnote{XMol is under
        \copyright\ copyright of Research Equipment Inc., dba
        Minnesota Supercomputer Center Inc.} Coordinates come out in
    \AA ngstr\"om independently of what specified in
    \fdf{AtomicCoordinatesFormat} and in
    \fdf{AtomCoorFormatOut}. There is a present \method{Java}
    implementation of \method{XMol} called \method{JMol}.

  \end{fdflogicalF}

  \begin{fdflogicalF}{WriteCoorCerius}
    \index{Cerius2@\textsc{Cerius2}}

    If \fdftrue it originates the writing of an extra file named
    \sysfile{xtl} containing the final atomic coordinates in a format
    directly readable by \method{Cerius}.\footnote{\method{Cerius} is
        under \copyright\ copyright of Molecular Simulations Inc.}
    Coordinates come out in \fdf*{Fractional} format (the same as
    \fdf*{ScaledByLatticeVectors}) independently of what specified
    in \fdf{AtomicCoordinatesFormat} and in
    \fdf{AtomCoorFormatOut}.  If negative coordinates are to be
    avoided, it has to be done from the start by shifting all the
    coordinates rigidly to have them positive, by using
    \fdf{AtomicCoordinatesOrigin}.  See the
    \program{Sies2arc}\index{Sies2arc@\textsc{Sies2arc}} utility in the
    \program{Util/} directory for generating \sysfile*{arc} files for CERIUS animation.

  \end{fdflogicalF}

  \begin{fdflogicalF}{WriteMDXmol}
    \index{XMol@\textsc{XMol}}
    \index{Molden@\textsc{Molden}}

    If \fdftrue\ it causes the writing of an extra file
    named \sysfile{ANI} containing all the atomic
    coordinates of the simulation in a format directly readable by
    \method{XMol} for animation.\index{animation} Coordinates come out in
    \AA ngstr\"om independently of what is specified in
    \fdf{AtomicCoordinatesFormat} and in \fdf{AtomCoorFormatOut}.
    This file is accumulative even for different runs.

    There is an alternative for animation by generating a \sysfile*{arc} file for
    \method{CERIUS}. It is through the
    \method{Sies2arc}\index{Sies2arc@\program{Sies2arc}} postprocessing
    utility in the \shell{Util/} directory, and it
    requires the coordinates to be accumulated in the output file, i.e.,
    \fdf{WriteCoorStep} \fdftrue.

  \end{fdflogicalF}

\end{itemize}



\subsubsection{Input of structural information from external files}

The structural information can be also read from external files. Note
that \fdf{ChemicalSpeciesLabel} is mandatory in the fdf file.

\begin{fdflogicalF}{MD!UseSaveXV}
  \index{reading saved data!XV}

  Logical variable which instructs \siesta\ to read the atomic
  positions and velocities stored in file \sysfile{XV} by a previous
  run.

  If the file does not exist, a warning is printed but the
  program does not stop. Overrides \fdf{UseSaveData}, but can be
  implicitly set by it.

\end{fdflogicalF}

\begin{fdflogicalF}{UseStructFile}

  Controls whether the structural information is read from an external
  file of name \sysfile{STRUCT\_IN}. If \fdftrue, all other
  structural information in the fdf file will be ignored.

  The format of the file is implied by the following code:
\begin{verbatim}
read(*,*) ((cell(ixyz,ivec),ixyz=1,3),ivec=1,3)  ! Cell vectors, in Angstroms
read(*,*) na
do ia = 1,na
   read(iu,*) isa(ia), dummy, xfrac(1:3,ia)  ! Species number
                                             ! Dummy numerical column
                                             ! Fractional coordinates
enddo
\end{verbatim}

  \textit{Warning:} Note that the resulting geometry could be clobbered if
  an \sysfile*{XV} file is read after this file. It is up to the user to remove
  any \sysfile*{XV} files.

\end{fdflogicalF}

\begin{fdflogicalF}{MD!UseSaveZM}
  \index{reading saved data!ZM}

  Instructs to read the Zmatrix information stored
  in file \sysfile*{ZM} by a previous run.

  If the required file does not exist, a warning is
  printed but the program does not stop. Overrides \fdf{UseSaveData},
  but can be implicitly set by it.

  \textit{Warning:} Note that the resulting geometry could be clobbered if
  an \sysfile*{XV} file is read after this file. It is up to the user to remove
  any \sysfile*{XV} files.

\end{fdflogicalF}



\subsubsection{Input from a FIFO file}

See the ``Forces'' option in \fdf{MD.TypeOfRun}. Note that
\fdf{ChemicalSpeciesLabel} is still mandatory in the fdf file.

\subsubsection{Precedence issues in structural input}
\index{structure input precedence issues}

\begin{itemize}
  \item If the ``Forces'' option is active, it takes precedence over
  everything (it will overwrite all other input with the information it
  gets from the FIFO file).

  \item If \fdf{MD!UseSaveXV} is active, it takes precedence over the options below.

  \item If \fdf*{MD!UseStructFile} (or \fdf{UseStructFile}) is active, it takes precedence
  over the options below.

  \item For atomic coordinates, the traditional and Zmatrix formats in
  the fdf file are mutually exclusive. If \fdf{MD!UseSaveZM} is
  active, the contents of the ZM file, if found, take precedence over
  the Zmatrix information in the fdf file.

\end{itemize}

\subsubsection{Interatomic distances}

\begin{fdfentry}{WarningMinimumAtomicDistance}[length]<$1\,\mathrm{Bohr}$>

  Fixes a threshold interatomic distance below which a warning
  message is printed.

\end{fdfentry}

\begin{fdfentry}{MaxBondDistance}[length]<$6\,\mathrm{Bohr}$>

  \siesta\ prints the interatomic distances\index{interatomic
      distances}, up to a range of \fdf{MaxBondDistance}, to file
  \sysfile{BONDS} upon first reading the structural information, and
  to file \sysfile{BONDS\_FINAL} after the last geometry
  iteration. The reference atoms are all the atoms in the unit
  cell. The routine now prints the real location of the neighbor atoms
  in space, and not, as in earlier versions, the location of the
  equivalent representative in the unit cell.

\end{fdfentry}