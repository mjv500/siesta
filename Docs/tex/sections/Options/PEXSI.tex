The PEXSI solver is based on the combination of the pole expansion of
the Fermi-Dirac function and the computation of only a selected
(sparse) subset of the elements of the matrices $(H-z_lS)^{-1}$ at
each pole $z_l$.

This solver can efficiently use the sparsity pattern of
the Hamiltonian and overlap matrices generated in SIESTA, and for
large systems has a much lower computational complexity than that
associated with the matrix diagonalization procedure. It is also
highly scalable.

This section applies to the native interface to the PEXSI library. There is another PEXSI interface offered through the ELSI
library of solvers (see Section~\ref{SolverELSI}).

The PEXSI technique can be used in this version of \siesta\ to evaluate the electron density, free
energy, atomic forces, density of states and local density of states
without computing any eigenvalue or eigenvector of the Kohn-Sham
Hamiltonian. It can achieve an accuracy fully comparable to that obtained
from a matrix diagonalization procedure for general systems, including
metallic systems at low temperature.

The current implementation of the PEXSI solver in \siesta\ makes use
of a full fine-grained-level interface to the PEXSI library (\url{http://pexsi.org}), and can deal
with (collinear) spin-polarization, but it is still restricted to $\Gamma$-point
calculations.  The PEXSI interface offered by ELSI offers some more options, although not currently the
density-of-states calculation.


The following is a brief description of the input-file parameters
relevant to the workings of the (native interface) PEXSI solver. For more background,
including a discussion of the conditions under which this solver is
competitive, the user is referred to the paper \citet{Lin2014}, and
references therein.

The technology involved in the PEXSI solver can also be used
to compute densities of states and ``local densities of
states''. These features are documented in this section and also
linked to in the relevant general sections.

\subsubsection{Pole handling}

Note that the temperature for the Fermi-Dirac distribution which is
pole-expanded is taken directly from the \fdf{ElectronicTemperature}
parameter (see Sec.~\ref{electronic-occupation}).


\begin{fdfentry}{PEXSI!NumPoles}[integer]<40>

  Effective number of poles used to expand the Fermi-Dirac function.

  When using the pole-generation method used in this legacy interface
  (contour integral), the allowed values for NumPoles are: 10, 20, 30,
  ..., 110, and 120.  Typically 60 to 100 poles are needed to get an
  accuracy comparable to diagonalization.

\end{fdfentry}

\begin{fdfentry}{PEXSI!deltaE}[energy]<$3\,\mathrm{Ry}$>

  In principle \fdf{PEXSI!deltaE} should be $E_{\max}-\mu$, where
  $E_{\max}$ is the largest eigenvalue for ($H$,$S$), and $\mu$ is the
  chemical potential. However, due to the fast decay of the
  Fermi-Dirac function, \fdf{PEXSI!deltaE} can often be chosen to be
  much lower.  In practice we set the default to be 3 Ryd.  This
  number should be set to be larger if the difference between
  $\Tr[\mathrm H\cdot\mathrm{DM}]$ and $\Tr[\mathrm S*\mathrm{EDM}]$
  (displayed in the output if \fdf{PEXSI!Verbosity} is at least 2)
  does not decrease with the increase of the number of poles.

\end{fdfentry}


\begin{fdfentry}{PEXSI!Gap}[energy]<$0\,\mathrm{Ry}$>

  Spectral gap. This can be set to be 0 in most cases.

\end{fdfentry}


\subsubsection{Parallel environment and control options}

\begin{fdfentry}{MPI!Nprocs.SIESTA}[integer]<\nonvalue{total processors}>

  Specifies the number of MPI processes to be used in those parts of
  the program (such as Hamiltonian setup and computation of forces)
  which are outside of the PEXSI solver itself. This is needed in
  large-scale calculations, for which the number of processors that
  can be used by the PEXSI solver is much higher than those needed by
  other parts of the code.

  Note that when the PEXSI solver is not used, this parameter will
  simply reduce the number of processors actually used by all parts of
  the program, leaving the rest idle for the whole calculation. This
  will adversely affect the computing budget, so take care not to use
  this option in that case.

\end{fdfentry}

\begin{fdfentry}{PEXSI!NP-per-pole}[integer]<4>

  Number of MPI processes used to perform the PEXSI computations in
  one pole. If the total number of MPI processes is smaller than this
  number times the number of poles (times the spin multiplicity), the
  PEXSI library will compute appropriate groups of poles in
  sequence. The minimum time to solution is achieved by increasing
  this parameter as much as it is reasonable for parallel efficiency,
  and using enough MPI processes to allow complete parallelization
  over poles. On the other hand, the minimum computational cost (in
  the sense of computing budget) is obtained by using the minimum
  value of this parameter which is compatible with the memory
  footprint. The additional parallelization over poles will be
  irrelevant for cost, but it will obviously affect the time to
  solution.

  Internally, \siesta\ computes the processor grid parameters
  \shell{nprow} and \shell{npcol} for the PEXSI library, with
  \shell{nprow} $>$= \shell{npcol}, and as similar as possible. So it
  is best to choose \fdf{PEXSI!NP-per-pole} as the product of two
  similar numbers.

  \note The total number of MPI processes must be divisible by
  \fdf{PEXSI!NP-per-pole}. In case of spin-polarized calculations, the
  total number of MPI processes must be divisible by
  \fdf{PEXSI!NP-per-pole} times 2.

\end{fdfentry}

\begin{fdfentry}{PEXSI!Ordering}[integer]<1>

  For large matrices, symbolic factorization should be performed in
  parallel to reduce the wall clock time.  This can be done using
  ParMETIS/PT-Scotch by setting \fdf{PEXSI!Ordering} to 0.  However,
  we have been experiencing some instability problem of the symbolic
  factorization phase when ParMETIS/PT-Scotch is used.  In such case,
  for relatively small matrices one can either use the sequential
  METIS (\fdf{PEXSI!Ordering} = 1) or set \fdf{PEXSI!NP-symbfact} to
  1.

\end{fdfentry}


\begin{fdfentry}{PEXSI!NP-symbfact}[integer]<1>

  Number of MPI processes used to perform the symbolic factorizations
  needed in the PEXSI procedure.  A default value should be given to
  reduce the instability problem.  From experience so far setting this
  to be 1 is most stable, but going beyond 64 does not usually improve
  much.

\end{fdfentry}

\begin{fdfentry}{PEXSI!Verbosity}[integer]<1>

  It determines the amount of information logged by the solver in
  different places. A value of zero gives minimal information.
  \begin{itemize}

    \item%
    In the files logPEXSI[0-9]+, the verbosity level is interpreted by
    the PEXSI library itself. In the latest version, when PEXSI is
    compiled in RELEASE mode, only logPEXSI0 is given in the output.
    This is because we have observed that simultaneous output for all
    processors can have very significant cost for a large number of
    processors ($>$10000).

    \item%
    In the SIESTA output file, a verbosity level of 1 and above will
    print lines (prefixed by \shell{\&o}) indicating the various heuristics
    used at each scf step. A verbosity level of 2 and above will print
    extra information.

  \end{itemize}
  The design of the output logging is still in flux.

\end{fdfentry}

\subsubsection{Electron tolerance and the PEXSI solver}


\begin{fdfentry}{PEXSI!num-electron-tolerance}[real]<$10^{-4}$>

  Tolerance in the number of electrons for the PEXSI solver. At each
  iteration of the solver, the number of electrons is computed as the
  trace of the density matrix times the overlap matrix, and compared
  with the total number of electrons in the system. This tolerance can
  be fixed, or dynamically determined as a function of the degree of
  convergence of the self-consistent-field loop.

\end{fdfentry}

\begin{fdfentry}{PEXSI!num-electron-tolerance-lower-bound}[real]<$10^{-2}$>

  See \fdf{PEXSI!num-electron-tolerance-upper-bound}.

\end{fdfentry}

\begin{fdfentry}{PEXSI!num-electron-tolerance-upper-bound}[real]<$0.5$>

  The upper and lower bounds for the electron tolerance are used to
  dynamically change the tolerance in the PEXSI solver, following the
  simple algorithm:
\begin{verbatim}
  tolerance = Max(lower_bound,Min(dDmax, upper_bound))
\end{verbatim}
  The first scf step uses the upper bound of the tolerance range, and
  subsequent steps use progressively lower values, in correspondence
  with the convergence-monitoring variable \fdf*{dDmax}.

  \note This simple update schedule tends to work quite well. There is
  an experimental algorithm, documented only in the code itself, which
  allows a finer degree of control of the tolerance update.

\end{fdfentry}


\begin{fdfentry}{PEXSI!mu-max-iter}[integer]<$10$>

  Maximum number of iterations of the PEXSI solver. Note that in this
  implementation there is no fallback procedure if the solver fails to
  converge in this number of iterations to the prescribed
  tolerance. In this case, the resulting density matrix might still be
  re-normalized, and the calculation able to continue, if the
  tolerance for non normalized DMs is not set too tight. For example,
\begin{verbatim}
  # (true_no_electrons/no_electrons) - 1.0
    DM.NormalizationTolerance 1.0e-3
\end{verbatim}
  will allow a 0.1\% error in the number of electrons. For obvious
  reasons, this feature, which is also useful in connection with the
  dynamic tolerance update, should not be abused.

  If the parameters of the PEXSI solver are adjusted correctly
  (including a judicious use of inertia-counting to refine the $\mu$
  bracket), we should expect that the maximum number of solver
  iterations needed is around 3

\end{fdfentry}

\begin{fdfentry}{PEXSI!mu}[energy]<$-0.6\,\mathrm{Ry}$>

  The starting guess for the chemical potential for the PEXSI
  solver. Note that this value does not affect the initial $\mu$
  bracket for the inertia-count refinement, which is controlled by
  \fdf{PEXSI!mu-min} and \fdf{PEXSI!mu-max}. After an inertia-count
  phase, $\mu$ will be reset, and further iterations inherit this
  estimate, so this parameter is only relevant if there is no
  inertia-counting phase.

\end{fdfentry}

\begin{fdfentry}{PEXSI!mu-pexsi-safeguard}[energy]<$0.05\,\mathrm{Ry}$>

  \note This feature has been deactivated for now. The condition for
  starting a new phase of inertia-counting is that the Newton
  estimation falls outside the current bracket. The bracket is
  expanded accordingly.

  The PEXSI solver uses Newton's method to update the estimate of
  $\mu$.  If the attempted change in $\mu$ is larger than
  \fdf{PEXSI!mu-pexsi-safeguard}, the solver cycle is stopped and a
  fresh phase of inertia-counting is started.

\end{fdfentry}

\subsubsection{Inertia-counting}


\begin{fdfentry}{PEXSI!Inertia-Counts}[integer]<3>

  In a given scf step, the PEXSI procedure can optionally employ a
  $\mu$ bracket-refinement procedure based on
  inertia-counting. Typically, this is used only in the first few scf
  steps, and this parameter determines how many. If positive,
  inertia-counting will be performed for exactly that number of scf
  steps. If negative, inertia-counting will be performed for at least
  that number of scf steps, and then for as long as the scf cycle is
  not yet deemed to be near convergence (as determined by the
  \fdf{PEXSI!safe-dDmax-no-inertia} parameter).

  \note Since it is cheaper to perform an inertia-count phase than to
  execute one iteration of the solver, it pays to call the solver only
  when the $\mu$ bracket is sufficiently refined.

\end{fdfentry}

\begin{fdfentry}{PEXSI!mu-min}[energy]<$-1\,\mathrm{Ry}$>

  The lower bound of the initial range for $\mu$ used in the
  inertia-count refinement. In runs with multiple geometry iterations,
  it is used only for the very first scf iteration at the first
  geometry step. Further iterations inherit possibly refined values of
  this parameter.

\end{fdfentry}

\begin{fdfentry}{PEXSI!mu-max}[energy]<$0\,\mathrm{Ry}$>

  The upper bound of the initial range for $\mu$ used in the
  inertia-count refinement. In runs with multiple geometry iterations,
  it is used only for the very first scf iteration at the first
  geometry step. Further iterations inherit possibly refined values of
  this parameter.

\end{fdfentry}

\begin{fdfentry}{PEXSI!safe-dDmax-no-inertia}[real]<0.05>

  During the scf cycle, the variable conventionally called
  \fdf*{dDmax} monitors how far the cycle is from convergence. If
  \fdf{PEXSI!Inertia-Counts} is negative, an inertia-counting phase
  will be performed in a given scf step for as long as \fdf*{dDmax} is
  greater than \fdf{PEXSI!safe-dDmax-no-inertia}.

  \note Even though \fdf*{dDmax} represents historically how far from
  convergence the density-matrix is, the same mechanism applies to
  other forms of mixing in which other magnitudes are monitored for
  convergence (Hamiltonian, charge density...).

\end{fdfentry}

\begin{fdfentry}{PEXSI!lateral-expansion-inertia}[energy]<$3\,\mathrm{eV}$>

  If the correct $\mu$ is outside the bracket provided to the
  inertia-counting phase, the bracket is expanded in the appropriate
  direction(s) by this amount.

\end{fdfentry}

\begin{fdfentry}{PEXSI!Inertia-mu-tolerance}[energy]<$0.05\,\mathrm{Ry}$>

  One of the criteria for early termination of the inertia-counting
  phase.  The value of the estimated $\mu$ (basically the center of
  the resulting brackets) is monitored, and the cycle stopped if its
  change from one iteration to the next is below this parameter.

\end{fdfentry}

\begin{fdfentry}{PEXSI!Inertia-max-iter}[integer]<$5$>

  Maximum number of inertia-count iterations per cycle.

\end{fdfentry}

\begin{fdfentry}{PEXSI!Inertia-min-num-shifts}[integer]<$10$>

  Minimum number of sampling points for inertia counts.

\end{fdfentry}

\begin{fdfentry}{PEXSI!Inertia-energy-width-tolerance}[energy]<\fdfvalue{PEXSI!Inertia-mu-tolerance}>

  One of the criteria for early termination of the inertia-counting
  phase.  The cycle stops if the width of the resulting bracket is
  below this parameter.

\end{fdfentry}


\subsubsection{Re-use of \texorpdfstring{$\mu$}{u} information accross iterations}

This is an important issue, as the efficiency of the PEXSI procedure
depends on how close a guess of $\mu$ we have at our
disposal. There are two types of information re-use:

\begin{itemize}
  \item%
  Bracketing information used in the inertia-counting phase.

  \item%
  The values of $\mu$ itself for the solver.
\end{itemize}

\begin{fdfentry}{PEXSI!safe-width-ic-bracket}[energy]<$4\,\mathrm{eV}$>

  By default, the $\mu$ bracket used for the inertia-counting phase in
  scf steps other than the first is taken as an interval of width
  \fdf{PEXSI!safe-width-ic-bracket} around the latest estimate of
  $\mu$.

\end{fdfentry}


\begin{fdfentry}{PEXSI!safe-dDmax-ef-inertia}[real]<$0.1$>

  The change in $\mu$ from one scf iteration to the next can be
  crudely estimated by assuming that the change in the band structure
  energy (estimated as Tr$\Delta H$DM) is due to a rigid shift.  When
  the scf cycle is near convergence, this $\Delta\mu$ can be used to
  estimate the new initial bracket for the inertia-counting phase,
  rigidly shifting the output bracket from the previous scf step.  The
  cycle is assumed to be near convergence when the monitoring variable
  \fdf*{dDmax} is smaller than \fdf{PEXSI!safe-dDmax-ef-inertia}.

  \note Even though \fdf*{dDmax} represents historically how far from
  convergence the density-matrix is, the same mechanism applies to
  other forms of mixing in which other magnitudes are monitored for
  convergence (Hamiltonian, charge density...).

  NOTE: This criterion will lead in general to tighter brackets than
  the previous one, but oscillations in H in the first few iterations
  might make it more dangerous. More information from real use cases
  is needed to refine the heuristics in this area.

\end{fdfentry}

\begin{fdfentry}{PEXSI!safe-dDmax-ef-solver}[real]<0.05>

  When the scf cycle is near convergence, the $\Delta\mu$ estimated as
  above can be used to shift the initial guess for $\mu$ for the PEXSI
  solver.  The cycle is assumed to be near convergence when the
  monitoring variable \fdf*{dDmax} is smaller than \fdf{PEXSI!safe-dDmax-ef-solver}.

  \note Even though \fdf*{dDmax} represents historically how far from
  convergence the density-matrix is, the same mechanism applies to
  other forms of mixing in which other magnitudes are monitored for
  convergence (Hamiltonian, charge density...).

\end{fdfentry}

\begin{fdfentry}{PEXSI!safe-width-solver-bracket}[energy]<$4\,\mathrm{eV}$>

  In all cases, a ``safe'' bracket around $\mu$ is provided even in
  direct calls to the PEXSI solver, in case a fallback to executing
  internally a cycle of inertia-counting is needed. The size of the
  bracket is given by \fdf{PEXSI!safe-width-solver-bracket}

\end{fdfentry}

\subsubsection{Calculation of the density of states by
  inertia-counting}
\label{pexsi-dos}

The cumulative or integrated density of states (INTDOS) can be easily
obtained by inertia-counting, which involves a factorization of
$H-\sigma S$ for varying $\sigma$ (see SIESTA-PEXSI paper).  Apart
from the DOS-specific options below, the ``ordering'', ``symbolic
factorization'', and ``pole group size'' (re-interpreted as the number
of MPI processes dealing with a given $\sigma$) options are honored.

The current version of the code generates a file with the
energy-INTDOS information, \file{PEXSI\_INTDOS}, which can be later
processed to generate the DOS by direct numerical differentiation, or
a \siesta-style \sysfile{EIG} file (using the \program{Util/PEXSI/intdos2eig}
program).

\begin{fdflogicalF}{PEXSI!DOS}

  Whether to compute the DOS (actually, the INTDOS --- see above)
  using the PEXSI technology.

\end{fdflogicalF}

\begin{fdfentry}{PEXSI!DOS!Emin}[energy]<$-1\,\mathrm{Ry}$>

  Lower bound of energy window to compute the DOS in.

  See \fdf{PEXSI!DOS!Ef.Reference}.

\end{fdfentry}

\begin{fdfentry}{PEXSI!DOS!Emax}[energy]<$1\,\mathrm{Ry}$>

  Upper bound of energy window to compute the DOS in.

  See \fdf{PEXSI!DOS!Ef.Reference}.

\end{fdfentry}

\begin{fdflogicalT}{PEXSI!DOS!Ef.Reference}

  If this flag is true, the bounds of the energy window
  (\fdf{PEXSI!DOS!Emin} and \fdf{PEXSI!DOS!Emax}) are with respect to
  the Fermi level.

\end{fdflogicalT}

\begin{fdfentry}{PEXSI!DOS!NPoints}[integer]<200>

  The number of points in the energy interval at which the DOS is
  computed. It is rounded up to the nearest multiple of the number of
  available factorization groups, as the operations are perfectly
  parallel and there will be no extra cost involved.

\end{fdfentry}

\subsubsection{Calculation of the LDOS by selected-inversion}
\label{pexsi-ldos}

The local-density-of-states (LDOS) around a given reference energy
$\varepsilon$, representing the contribution to the charge density of
the states with eigenvalues in the vicinity of $\varepsilon$, can be
obtained formally by a ``one-pole expansion'' with suitable broadening
(see SIESTA-PEXSI paper).

Apart from the LDOS-specific options below, the ``ordering'',
``verbosity'', and ``symbolic factorization'' options are honored.

The current version of the code generates a real-space grid file with
extension \sysfile{LDOS}, and (if netCDF is compiled-in) a file
\file{LDOS.grid.nc}.

NOTE: The LDOS computed with this procedure is not exactly the same as
the vanilla \siesta\ LDOS, which uses an explicit energy
interval. Here the broadening acts around a single value of the
energy.


\begin{fdflogicalF}{PEXSI!LDOS}

  Whether to compute the LDOS using the PEXSI technology.

  \note this flag is not compatible with \fdf{LocalDensityOfStates}.

\end{fdflogicalF}

\begin{fdfentry}{PEXSI!LDOS!Energy}[energy]<$0\,\mathrm{Ry}$>

  The (absolute) energy at which to compute the LDOS.

\end{fdfentry}

\begin{fdfentry}{PEXSI!LDOS!Broadening}[energy]<$0.01\,\mathrm{Ry}$>

  The broadening parameter for the LDOS.

\end{fdfentry}

\begin{fdfentry}{PEXSI!LDOS!NP-per-pole}[integer]<\fdfvalue{PEXSI!NP-per-pole}>

  The value of this parameter supersedes \fdf{PEXSI!NP-per-pole} for
  the calculation of the LDOS, which otherwise would keep idle all but
  \fdf{PEXSI!NP-per-pole} MPI processes, as it essentially consists of
  a ``one-pole'' procedure.

\end{fdfentry}
