

\begin{fdfentry}{SystemLabel}[string]<siesta>

    A \emph{single} word (max. 20 characters \emph{without blanks})
    containing a nickname of the system, used to name output files.
  
  \end{fdfentry}
  
  
  \begin{fdfentry}{SystemName}[string]
  
    A string of one or several words containing a descriptive name of
    the system (max. 150 characters).
  
  \end{fdfentry}
  
  
  \begin{fdfentry}{NumberOfSpecies}[integer]<\nonvalue{lines in \fdf{ChemicalSpeciesLabel}}>
  
    Number of different atomic species in the simulation.  Atoms of the
    same species, but with a different pseudopotential or basis set are
    counted as different species.
  
    \note This is not required to be set.
  
  \end{fdfentry}
  
  \begin{fdfentry}{NumberOfAtoms}[integer]<\nonvalue{lines in \fdf{AtomicCoordinatesAndAtomicSpecies}}>
  
    Number of atoms in the simulation.
  
    \note This is not required to be set.
  
  \end{fdfentry}
  
  
  \begin{fdfentry}{ChemicalSpeciesLabel}[block]
  
    It specifies the different chemical species\index{species} that are
    present, assigning them a number for further identification.
    \siesta\ recognizes the different atoms by the given atomic number.
  
    \begin{fdfexample}
       %block ChemicalSpecieslabel
          1   6   C    pbe/C.psml
          2  14   Si
          3  14   Si_surface  Si.psf
       %endblock ChemicalSpecieslabel
    \end{fdfexample}
    The first number in a line is the species number, it is followed by
    the atomic number, and then by the desired unique label. This label
    will be used to identify each species. For example the label is the
    equivalent label name that should be found in the \fdf{PAO!Basis}
    block.
  
    This construction allows you to have atoms of the same species but
    with different basis or pseudopotential, for example.
  
    Optionally, a string \textit{ps-file-spec} after the species name
    determines the pseudopotential file to be used.  In the example
    above, the \shell{C} atoms will use the pseudopotential file
    \shell{pbe/C.psml} (with reference to the current directory), and
    both the \shell{Si} and the \shell{Si\_surface} species will use a
    pseudopotential file named \shell{Si.psf}. See
    section~\ref{ps-handling} for a full discussion of options.
  
    Negative atomic numbers are used for \emph{ghost} atoms\index{ghost
        atoms} (see \fdf{PAO!Basis}).
  
    For atomic numbers over $200$ or below $-200$ you should read
    \fdf{SyntheticAtoms}.
  
    \note This block is mandatory.
  
  \end{fdfentry}
  
  
  
  \begin{fdfentry}{SyntheticAtoms}[block]
  
    This block provides information about the ground-state valence configuration of
    a species. Its main use is to complement the information in
    \fdf{ChemicalSpeciesLabel} for \emph{synthetic} (alchemical)
     \index{synthetic atoms}
     species, which are represented by atomic numbers over $200$ in
     \fdf{ChemicalSpeciesLabel}. These species are created for example as a ``mixture'' of two
    real ones for a ``virtual crystal'' (VCA)\index{VCA}
    calculation. In this special case a new \fdf{SyntheticAtoms} block
    must be present to give \siesta\ information about the ``ground
    state'' of the synthetic atom.
  
    \begin{fdfexample}
       %block ChemicalSpeciesLabel
          1   201 ON-0.50000
       %endblock ChemicalSpeciesLabel
       %block SyntheticAtoms
          1               # Species index
          2 2 3 4         # n numbers for valence states  with l=0,1,2,3
          2.0 3.5 0.0 0.0 # occupations of valence states with l=0,1,2,3
       %endblock SyntheticAtoms
    \end{fdfexample}
  
    Pseudopotentials for synthetic atoms can be created using the
    \program{mixps} and \program{fractional} programs \index{mixps
        program}\index{fractional program} in the \shell{Util/VCA}
    directory.
  
    Atomic numbers below $-200$ represent \emph{ghost synthetic atoms}.
  
    Note that the procedure used in the automatic handling of semicore
    states does not work for synthetic atoms.  If semicore states are
    present, the species must be put in the \fdf{PAO!Basis} block.
    Otherwise the program will assume that there are \emph{no} semicore
    states.
  
    This block can also be used to provide an alternate ground state
    valence configuration for real atoms in some special cases.
    \index{valence configuration (alternate)}
    For example, the nominal valence configuration for Pd in the Siesta
    internal tables is 5s1 5p0 4d9 4f0, but in some tables it appears as
    5s0 5p0 4d10 4f0. In this case, the alternate configuration can be
    specified by the block:
  
    \begin{fdfexample}
       %block ChemicalSpeciesLabel
          1   46 Pd
       %endblock ChemicalSpeciesLabel
       %block synthetic-atoms
       1
         5 5 4 4
         0.0 0.0 10.0 0.0
       %endblock synthetic-atoms
    \end{fdfexample}
  
    As another example, the nominal valence for Cu in Siesta is
    4s1 4p0 3d10 4f0, but in some cases a pseudopotential might
    be generated by considering the 3d shell as frozen in the core.
    In this case the proper valence configuration is:
  
    \begin{fdfexample}
       %block ChemicalSpeciesLabel
          1   29 Cu_3d_in_core
       %endblock ChemicalSpeciesLabel
       %block synthetic-atoms
       1
         4 4 4 4
         1.0 0.0 0.0 0.0
       %endblock synthetic-atoms
    \end{fdfexample}
  
    As a final example, the nominal valence configuration for Ce in
    Siesta is 6s2 6p0 5d0 4f2, but on some tables it appears as [Xe] 6s2
    4f1 5d1. In addition, the pseudo-dojo pseudopotential (in the NC SR+3 table) has
    the 4f shell frozen in the core. This case can be handled by the
    block:
  
    \begin{fdfexample}
       %block ChemicalSpeciesLabel
          1   58 Ce_4f_in_core
       %endblock ChemicalSpeciesLabel
       %block synthetic-atoms
       1
         6 6 5 5
         2.0 0.0 1.0 0.0
       %endblock synthetic-atoms
    \end{fdfexample}
  
    Note that the change in the atomic ground-state configuration might
    change the choice of polarization orbitals, and possibly other
    Siesta heuristic decisions, so the results should be checked
    carefully.
  
  \end{fdfentry}
  
  \begin{fdfentry}{AtomicMass}[block]
  
    It allows the user to introduce the atomic masses of the different
    species used in the calculation, useful for the dynamics with
    isotopes,\index{isotopes} for example. If a species index is not
    found within the block, the natural mass for the corresponding
    atomic number is assumed. If the block is absent all masses are the
    natural ones. One line per species with the species index (integer)
    and the desired mass (real). The order is not important. If there is
    no integer and/or no real numbers within the line, the line is
    disregarded.
  
    \begin{fdfexample}
       %block AtomicMass
          3  21.5
          1  3.2
       %endblock AtomicMass
    \end{fdfexample}
  
    The default atomic mass are the natural masses. For \emph{ghost}
    atoms (i.e. floating orbitals) the mass is $10^{30}\,\mathrm{a.u.}$
  
  \end{fdfentry}
  
  
  